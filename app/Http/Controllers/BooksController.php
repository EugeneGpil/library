<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;

class BooksController extends Controller
{
    public function store(Request $request) 
    {
        if ($request->title == '' or $request->author == ''
            or $request->year <= 0){
                return "Missing some data";
            }

        $book = new Book;

        $book->title = $request->title;
        $book->author = $request->author;
        $book->year = $request->year;
        $book->in_library = $request->inLibrary;

        $book->save();

        return "Succes!";
    }



    public function getAll()
    {
        $books = Book::all();

        return response()->json($books);
    }



    public function search(Request $request)
    {
        if ($request->column == "inLibrary"){
            $request->column = "in_library";
            if ($request->search) {
                $request->search = 1;
            } else {
                $request->search = 0;
            }
        }

        $searchResult = Book::where($request->column, 'LIKE', '%' . $request->search . '%')->get();

        return response()->json($searchResult);
    }
}
