import NewBook from './components/NewBook.vue';
import Search from './components/Search.vue';

export const routes = [
    {
        path: '/newBook',
        component: NewBook
    },
    {
        path: '/',
        component: Search
    }
];